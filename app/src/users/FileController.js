(function(){

  angular
       .module('files')
       .controller('FileController', [
          'fileService', '$mdSidenav','$mdBottomSheet', '$log', '$q','$translate', 
          FileController])
.config(function($translateProvider) {
    $translateProvider.translations('en', {
    HEADLINE: 'Wisefox',
    INTRO_TEXT: 'Wisefox',
    BUTTON_TEXT_EN: 'english',
    BUTTON_TEXT_RU: 'russian'
  })
  .translations('ru', {
    HEADLINE: 'Вайзфокс',
    INTRO_TEXT: 'Вайзфокс',
    BUTTON_TEXT_EN: 'английский',
    BUTTON_TEXT_RU: 'русский'
  });
  $translateProvider.preferredLanguage('ru');

});

  /**
   * Main Controller for the Angular Material Starter App
   * @param $scope
   * @param $mdSidenav
   * @param avatarsService
   * @constructor
   */
  function FileController( fileService, $mdSidenav, $mdBottomSheet, $log, $translate) {
    var self = this;
	
    self.selected     = null;
    self.files        = [ ];
    self.selectSearch   = selectSearch;
    self.selectOpt   = selectOpt;
    self.selectSaf   = selectSaf;
    self.selectLang  = selectLang;
    self.selectHist  = selectHist;
    self.selectReq  = selectReq;
    self.selectFile   = selectFile;
    self.changeLanguage = changeLanguage;
    self.toggleList   = toggleMenuList;
    self.makeContact  = makeContact;

    // Load all registered users

    fileService
          .loadAllFiles()
          .then( function( files ) {
            self.files    = [].concat(files);
            self.selected = files[0];
          });
    // *********************************
    // Internal methods
    // *********************************

    /**
     * Hide or Show the 'left' sideNav area
     */
    function toggleMenuList() {
      $mdSidenav('left').toggle();
    }
   function changeLanguage(langKey) {
    $translate.use(langKey);
  };

    /**
     *Заменить на одну функцию выбора, чтобы уменьшить код!!!
     */
function selectOpt() {
       	$mdBottomSheet.show({
          controllerAs  : "pp",
          templateUrl   : './src/users/view/profileSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            { name: 'Richard'       ,surname: 'Newfish' ,  icon: 'avatar'       , icon_url: 'assets/svg/avatar-1.svg', email:'rich@gmail.com'},
          ];
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };

        }
	
}

function selectSaf() {
       	$mdBottomSheet.show({
          controllerAs  : "sf",
          templateUrl   : './src/users/view/safetySheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            { name: 'Richard'       ,surname: 'Newfish' ,  icon: 'avatar'       , icon_url: 'assets/svg/avatar-1.svg', password:'12345'},
          ];
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
}

function selectLang() {
       	$mdBottomSheet.show({
          controllerAs  : "ln",
          templateUrl   : './src/users/view/languageSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            { name: ''       ,surname: '' ,  icon: 'avatar'       , icon_url: 'assets/svg/avatar-1.svg', password:'12345'},
          ];
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
	  function changeLanguage (langKey) {
	    $translate.use(langKey);
	  };
}
}
function selectHist() {
       	$mdBottomSheet.show({
          controllerAs  : "hs",
          templateUrl   : './src/users/view/historySheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            { hist: 'Посещение страницы профиль', date:'12.12.12 03:12:11'},
	    { hist: 'Поиск data', date:'12.12.12 03:12:11'},
          ];
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
}
function selectReq() {
       	$mdBottomSheet.show({
          controllerAs  : "rq",
          templateUrl   : './src/users/view/requestSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            { request: '1', date:'12.12.12 03:12:11',user:'Richard Newfish',status:'Stop'},
	    { request: '2', date:'12.12.12 03:12:11',user:'Richard Newfish',status:'Go'},
          ];
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
}

function selectSearch(query) {
       	$mdBottomSheet.show({
          controllerAs  : "ss",
          templateUrl   : './src/users/view/searchSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });
	function ContactSheetController( $mdBottomSheet ) {
          this.actions = [
            {}
          ];
	this.files = self.files ;
	this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
}

    function selectFile( file ) {
      self.selected = angular.isNumber(file) ? $scope.files[file] : file;

	$mdBottomSheet.show({
          controllerAs  : "fp",
          templateUrl   : './src/users/view/fileSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });

        /**
         * User ContactSheet controller
         */
        function ContactSheetController( $mdBottomSheet ) {
          this.file= self.selected;
          this.actions = [
            { name: 'Phone'       , icon: 'phone'       , icon_url: 'assets/svg/phone.svg'},
            { name: 'Twitter'     , icon: 'twitter'     , icon_url: 'assets/svg/twitter.svg'},
            { name: 'Google+'     , icon: 'google_plus' , icon_url: 'assets/svg/google_plus.svg'},
            { name: 'Hangout'     , icon: 'hangouts'    , icon_url: 'assets/svg/hangouts.svg'}
          ];
          this.cancel = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
    }

    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedFile) {

        $mdBottomSheet.show({
          controllerAs  : "fp",
          templateUrl   : './src/users/view/fileSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });

        /**
         * User ContactSheet controller
         */
        function ContactSheetController( $mdBottomSheet ) {
          this.file= selectedFile;
          this.actions = [
            { name: 'Phone'       , icon: 'phone'       , icon_url: 'assets/svg/phone.svg'},
            { name: 'Twitter'     , icon: 'twitter'     , icon_url: 'assets/svg/twitter.svg'},
            { name: 'Google+'     , icon: 'google_plus' , icon_url: 'assets/svg/google_plus.svg'},
            { name: 'Hangout'     , icon: 'hangouts'    , icon_url: 'assets/svg/hangouts.svg'}
          ];
          this.contactFile = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
    }

  }

})();
