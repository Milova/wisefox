(function(){
  'use strict';

  // Prepare the 'files' module for subsequent registration of controllers and delegates
  angular.module('files', [ 'ngMaterial', 'md.data.table','pascalprecht.translate' ]);
})();
